# Single-Page Web Apps in Elm

## Elm Make

To just build project execute following command:

    elm make src/Main.elm
    Success! Compiled 1 module.
    Successfully generated index.html

This will generate `index.html` which can be bundled with all generated JavaScript code. `elm-make` supports `--output` flag. You can tell `elm-make` to output either an HTML file or a JavaScript file. If you give HTML file, similar page will be generated but with given name.

If you just want to generate JavaScript, execute following command:

    elm make src/Main.elm --output=app.js
    Success! Compiled 1 module.
    Successfully generated app.js

This will generate only JavaScript. You have to make your own HTML page, something like this:

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Blog Thing</title>
    </head>
    <body>
        <script src="app.js"></script>
        <script>
            Elm.Main.fullscreen();
        </script>
    </body>
    </html>
